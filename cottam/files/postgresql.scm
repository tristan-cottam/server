(define-module (cottam files postgresql)
  #:use-module (guix gexp)
  #:export (pg_hba.conf))

(define pg_hba.conf
  (local-file "postgresql/pg_hba.conf"))
