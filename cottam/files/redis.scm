(define-module (cottam files redis)
  #:use-module (guix gexp)
  #:export (redis.conf))

(define redis.conf
  (local-file "redis/redis.conf"))
