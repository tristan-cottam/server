(define-module (cottam files ssh)
  #:use-module (guix gexp)
  #:export (tristan-cottam.pub))

(define tristan-cottam.pub
  (local-file "ssh/keys/tristan-cottam.pub"))
