(define-module (cottam machines cottam-server)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)
  #:use-module (cottam operating-systems cottam-server)
  #:export (cottam-server-machines))

(define cottam-server-machines
  (list (machine
         (operating-system cottam-server)
         (environment managed-host-environment-type)
         (configuration (machine-ssh-configuration
                         (host-name "ssh.cott.am")
                         (system "x86_64-linux")
                         (port 2222)
                         (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOzPyXRQbxWbj/fzrPpQxjyVEhiV8NGnfwewYzGNBSyS cott.am"))))))

cottam-server-machines
