(define-module (cottam operating-systems cottam-server)
  #:use-module (guix gexp)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu system)
  #:use-module (gnu system accounts)
  #:use-module (gnu system file-systems)
  #:use-module (gnu system keyboard)
  #:use-module (gnu system mapped-devices)
  #:use-module (gnu system shadow)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services certbot)
  #:use-module (gnu services cups)
  #:use-module (gnu services databases)
  #:use-module (gnu services dbus)
  #:use-module (gnu services dns)
  #:use-module (gnu services desktop)
  #:use-module (gnu services docker)
  #:use-module (gnu services mcron)
  #:use-module (gnu services networking)
  #:use-module (gnu services ssh)
  #:use-module (gnu services web)
  #:use-module (gnu packages)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages shells)
  #:use-module (cottam files postgresql)
  #:use-module (cottam files redis)
  #:use-module (cottam files ssh)
  #:export (cottam-server))

(define cottam-server
  (operating-system
    (host-name "cottam-server")
    (locale "en_US.utf8")
    (timezone "Europe/Brussels")
    (keyboard-layout (keyboard-layout "us" "altgr-intl"))
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (targets (list "/dev/nvme0n1"))
                 (keyboard-layout keyboard-layout)))
    (mapped-devices (list (mapped-device
                           (source (uuid
                                    "cdf031b9-f236-42a8-a347-240827a8bcbf"))
                           (target "cryptroot")
                           (type luks-device-mapping))))
    (file-systems (cons* (file-system
                           (mount-point "/")
                           (device "/dev/mapper/cryptroot")
                           (type "btrfs")
                           (dependencies mapped-devices)) %base-file-systems))
  (users (cons* (user-account
                 (name "tristan")
                 (comment "Tristan")
                 (group "users")
                 (home-directory "/home/tristan")
                 (supplementary-groups '("wheel" "netdev" "audio" "video" "docker"))
                 (shell (file-append zsh "/bin/zsh")))
                %base-user-accounts))
    (packages (append (specifications->packages (list "docker-compose"
                                                      "redis"
                                                      "nss-certs"))
                      %base-packages))
    (services
     (append (list (service openssh-service-type
                            (openssh-configuration
                             (port-number 2222)
                             (password-authentication? #f)
                             (permit-root-login #t)
                             (authorized-keys
                              `(("root" ,tristan-cottam.pub)
                                ("tristan" ,tristan-cottam.pub)))))
                   (service dhcp-client-service-type)
                   (service ntp-service-type)
                   (service cups-service-type)
                   (service elogind-service-type) ;required for Docker
                   (service dbus-root-service-type) ;required for Docker
                   (service docker-service-type)
                   (service certbot-service-type
                            (certbot-configuration
                             (email "tristan@cott.am")
                             (certificates
                              (list
                               (certificate-configuration
                                (domains '("cott.am"
                                           "cloud.cott.am"
                                           "home.cott.am"
                                           "search.cott.am"))
                                (deploy-hook (program-file
                                              "nginx-deploy-hook"
                                              #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
                                                  (kill pid SIGHUP)))))))))
                   (service nginx-service-type
                            (nginx-configuration
                             (server-blocks
                              (list (nginx-server-configuration
                                     (server-name '("cloud.cott.am"))
                                     (listen '("443 ssl"))
                                     (ssl-certificate "/etc/letsencrypt/live/cott.am/fullchain.pem")
                                     (ssl-certificate-key "/etc/letsencrypt/live/cott.am/privkey.pem")
                                     (locations
                                      (list (nginx-location-configuration
                                             (uri "/")
                                             (body '("proxy_pass http://localhost:8080;"
                                                     "proxy_set_header Host $http_host;"
                                                     "proxy_set_header X-Real-IP $remote_addr;"
                                                     "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                                                     "proxy_set_header X-Forwarded-Proto $scheme;"
                                                     "client_max_body_size 10G;")))
                                            (nginx-location-configuration
                                             (uri "/.well-known/caldav")
                                             (body '("return 301 $scheme://$host/remote.php/dav;")))
                                            (nginx-location-configuration
                                             (uri "/.well-known/carddav")
                                             (body '("return 301 $scheme://$host/remote.php/dav;"))))))
                                    (nginx-server-configuration
                                     (server-name '("home.cott.am"))
                                     (listen '("443 ssl"))
                                     (ssl-certificate "/etc/letsencrypt/live/cott.am/fullchain.pem")
                                     (ssl-certificate-key "/etc/letsencrypt/live/cott.am/privkey.pem")
                                     (locations
                                      (list (nginx-location-configuration
                                             (uri "/")
                                             (body '("proxy_pass http://localhost:8123;"
                                                     "proxy_set_header Host $http_host;"
                                                     "proxy_set_header X-Real-IP $remote_addr;"
                                                     "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                                                     "proxy_set_header X-Forwarded-Proto $scheme;"
                                                     ;Enable WebSocket
                                                     "proxy_set_header Upgrade $http_upgrade;"
                                                     "proxy_set_header Connection \"upgrade\";"))))))
                                    (nginx-server-configuration
                                     (server-name '("search.cott.am"))
                                     (listen '("443 ssl"))
                                     (ssl-certificate "/etc/letsencrypt/live/cott.am/fullchain.pem")
                                     (ssl-certificate-key "/etc/letsencrypt/live/cott.am/privkey.pem")
                                     (locations
                                      (list (nginx-location-configuration
                                             (uri "/")
                                             (body '("proxy_pass http://localhost:8081;"
                                                     "proxy_set_header Host $http_host;"
                                                     "proxy_set_header X-Real-IP $remote_addr;"
                                                     "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;"
                                                     "proxy_set_header X-Forwarded-Proto $scheme;"))))))))))
                   (service postgresql-service-type
                            (postgresql-configuration
                             (postgresql postgresql-15)
                             (config-file
                              (postgresql-config-file
                               (hba-file pg_hba.conf)
                               (extra-config
                                '(("listen_addresses" "*")))))))
                   (service postgresql-role-service-type
                            (postgresql-role-configuration
                             (roles
                              (list (postgresql-role
                                     (name "nextcloud")
                                     (create-database? #t))
                                    (postgresql-role
                                     (name "minetest")
                                     (create-database? #t))))))
                   (simple-service 'nextcloud-cron-job
                                   mcron-service-type
                                   (list #~(job '(next-minute (range 0 60 5))
                                                "docker exec -u 33 nextcloud php -f /var/www/html/cron.php")
                                         #~(job '(next-minute (range 0 60 5))
                                                "docker exec -u 33 nextcloud php -f /var/www/html/occ preview:pre-generate")))
                   (service redis-service-type
                            (redis-configuration
                             (config-file redis.conf))))
             (modify-services %base-services
               (guix-service-type config =>
                                  (guix-configuration
                                   (inherit config)
                                   (authorized-keys
                                    (append (list (local-file "/etc/guix/signing-key.pub"))
                                            %default-authorized-guix-keys)))))))))
